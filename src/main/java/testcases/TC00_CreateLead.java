package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC00_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDescription = "CreateLead";
		authors = "Bhuvana";
		category = "smoke";
		dataSheetName = "TC001";
		testNodes = "CreateLead";
	}
	
	@Test(dataProvider = "fetchData")
	public void login(String userName,String password, String cname, String fname, String lname) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(cname)
		.enterFirstName(fname)
		.enterSurname(lname)
		.clickCreateLead()
		.verifyFirstName(fname);
		
		
	}
	
}
