package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
    @FindBy(id = "createLeadForm_companyName") WebElement elecompanyname;
    @FindBy(id = "createLeadForm_firstName") WebElement elefirstname;
    @FindBy(id = "createLeadForm_lastName") WebElement elelastname;
	@FindBy(how = How.CLASS_NAME, using = "smallSubmit") WebElement elecreatelead;
    @Given("Enter the CompanyName as(.*)")
	public CreateLeadPage enterCompanyName(String data) {
		type(elecompanyname, data);
		return this;
	}
	@Given("Enter the FirstName as (.*)")
public CreateLeadPage enterFirstName(String data) {
	type(elefirstname, data);
	return this;

}
	@Given("Enter the LastName as (.*)")
public CreateLeadPage enterSurname(String data) {
	type(elelastname,data);
	return this;
	
}
	@Given("Click on the Create Lead Button")
public ViewLeadPage clickCreateLead() {
	elecreatelead.click();
	return new ViewLeadPage();
	
}

}
