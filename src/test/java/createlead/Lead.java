//package createlead;
//
//import java.util.concurrent.TimeUnit;
//
//import org.openqa.selenium.chrome.ChromeDriver;
//
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//
//public class Lead {
//	public ChromeDriver driver;
//	@Given("Open The Browser")
//	public void OpenBrowser() {
//		System.setProperty("webdriver.chrome.driver", "/home/bhuvaneshwaris/Desktop/chromedriver");
//		driver = new ChromeDriver();
//	}
//		@Given("Max the Browser")
//		public void maxTheBrowser() {
//		    driver.manage().window().maximize();
//		}
//
//		@Given("Set the Timeout")
//		public void setTheTimeout() {
//			driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
//		    
//		}
//
//		@Given("Launch the URL")
//		public void launchTheURL() {
//			driver.get("http://www.leaftaps.com/opentaps/");
//			
//		    
//		}
//
//		@Given("Enter the Username as (.*)")
//		public void enterTheUsernameAsDemoSalesManager(String data) {
//			driver.findElementById("username").sendKeys(data);
//		   
//		}
//
//		@Given("Enter the Password as (.*)")
//		public void enterThePasswordAsCrmsfa(String data) {
//			driver.findElementById("password").sendKeys(data);
//		    
//		}
//
//		@When("Click on the Login Button")
//		public void clickOnTheLoginButton() {
//			driver.findElementByClassName("decorativeSubmit").click();
//		    
//		}
//
//		@Then("Verify the Login")
//		public void verifyTheLogin() {
//			System.out.println("Success into MyHome Page");
//		    
//		}
//
//		@Then("Click on CRMSFA link")
//		public void clickOnCRMSFALink() {
//			driver.findElementByLinkText("CRM/SFA").click();
//		    
//		}
//
//		@Then("Click on MyLeads link")
//		public void clickOnMyLeadsLink() {
//			driver.findElementByLinkText("Leads").click();
//		    
//		}
//
//		@Then("Click on Create Lead")
//		public void clickOnCreateLead() {
//			driver.findElementByLinkText("Create Lead").click();
//		    
//		}
//
//		@Then("Enter the CompanyName as (.*)")
//		public void enterTheCompanyNameAsPolaris(String data) {
//			driver.findElementById("createLeadForm_companyName").sendKeys(data);
//		    
//		}
//
//		@Then("Enter the FirstName as (.*)")
//		public void enterTheFirstNameAsRaj(String data) {
//			driver.findElementById("createLeadForm_firstName").sendKeys(data);
//		    
//		}
//
//		@Then("Enter the LastName as (.*)")
//		public void enterTheLastNameAsSundar(String data) {
//			driver.findElementById("createLeadForm_lastName").sendKeys(data);
//		    
//		}
//
//		@When("Click on the Create Lead Button")
//		public void clickOnTheCreateLeadButton() {
//			driver.findElementByClassName("smallSubmit").click();
//		    
//		}
//
//		@Then("Verify the FirstName")
//		public void verifyTheFirstName() {
//			String cmp = driver.findElementById("viewLead_firstName_sp").getText();
//			if (cmp.contains("Raj"))
//				System.out.println("Printing the value");		
//			else
//				System.out.println("Not Printing the value");
//					
//			
//		    
//		}
//}